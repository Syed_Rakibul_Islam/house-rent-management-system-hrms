<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::group(['middleware' => ['web', 'auth']], function () {

    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index');

    /*
     * ******************
     * CRUDs
     * ******************
     */


    Route::get('notification', 'NotificationController@index');
    Route::get('notification/{id}', 'NotificationController@show');

    Route::resource('house/my_house', 'House\\MyHouseController');
    Route::resource('house/my_flat', 'House\\MyFlatController');
    Route::resource('house/renter', 'House\\RenterController');
    Route::post('house/renter/end/{id}', 'House\\RenterController@endRent');
    Route::get('house/previous_renter', 'House\\RenterController@previousRenter');
    Route::get('my_renting', 'Rent\\RentController@index');
    Route::get('my_renting/{id}', 'Rent\\RentController@show');
    Route::put('my_renting/{id}', 'Rent\\RentController@approve');
    Route::get('previous_renting', 'Rent\\RentController@previousRenting');


    Route::group(['middleware' => ['admin']], function () {
//        Route::resource('admin/role', 'Admin\\RoleController');
        Route::resource('master_data/address_type', 'MasterData\\AddressTypeController');
        Route::resource('user', 'UserController');
        Route::post('user/{id}/active', 'UserController@active');
        Route::post('user/{id}/suspend', 'UserController@suspend');
    });

});
Route::get('/test', 'HomeController@test');
