<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class MakeAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 1,
            'name' => 'Super Admin',
            'username' => 'admin',
            'password' => bcrypt('123456'),
            'father_name' => 'Mr. Y',
            'mother_name' => 'Mrs. X',
            'dob' => Carbon::now(),
            'gender' => 'Male',
            'marital_status' => 'Single',
            'religion' => 'Muslim',
            'occupation' => 'Student',
            'occupation_details' => 'Student',
            'educational_qualification' => 'BSc in CSE',
            'phone_number' => '+8801723000000',
            'email' => 'email@email.com',
            'nid' => '12345678909876524',
            'passport_number' => '123456789',
            'tin_number' => '12345',
            'permanent_address' => 'Dhaka, Bangladesh',
            'emergency_contact' => 'Mr. T; Dhaka, Bangladesh',
            'active' => 1,
            'type' => 'admin',

            'remember_token' => bcrypt(str_random(10)),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
