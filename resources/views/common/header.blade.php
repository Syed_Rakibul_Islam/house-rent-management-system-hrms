@php($notifications = \App\Notification::where(['user_id' => auth()->user()->id])->get())
<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <img src="{{ asset('design/images/img.jpg') }}" alt="">{{ Auth::user()->name }}
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
{{--                        <li><a href="javascript:;"> Profile</a></li>--}}
{{--                        <li>--}}
{{--                            <a href="javascript:;">--}}
{{--                                <span class="badge bg-red pull-right">50%</span>--}}
{{--                                <span>Settings</span>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li><a href="javascript:;">Help</a></li>--}}
                        <li><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                    </ul>
                </li>

                <li role="presentation" class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-envelope-o"></i>
                        @if($notifications->where('status', null)->count() > 0)
                            <span class="badge bg-green">{{ $notifications->where('status', null)->count() }}</span>
                        @endif
                    </a>
                    <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                        @foreach($notifications as $notification)
                            <li>
                                <a href="{{ url('notification/' . $notification->id) }}">
                                    <span>
                                        <span>
                                            <b>
                                                @if($notification->type == 'renter')
                                                    Renting Request
                                                @elseif($notification->type == 'rent_approve')
                                                    Renting Approved
                                                @else
                                                    Request
                                                @endif
                                            </b>
                                        </span>
                                      <span class="time">{{ $notification->created_at->diffForHumans() }}</span>
                                    </span>
                                    <span class="message">
                                      {{ $notification->name }}
                                    </span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
</div>