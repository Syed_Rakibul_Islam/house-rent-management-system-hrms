<div class="item form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name *', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('name', null, ['class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Enter Name', 'data-validate-length-range' => '3', 'required' => 'required']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@if(!isset($user))
<div class="item form-group {{ $errors->has('username') ? 'has-error' : ''}}">
    {!! Form::label('username', 'Username *', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('username', null, ['class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Enter Username', 'required' => 'required']) !!}
        {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="item form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Email *', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::email('email', null, ['class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Enter Email', 'required' => 'required']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="item form-group {{ $errors->has('password') ? 'has-error' : ''}}">
    {!! Form::label('password', 'Password *', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::password('password', ['class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Enter Password', 'required' => 'required']) !!}
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@endif
<div class="item form-group {{ $errors->has('father_name') ? 'has-error' : ''}}">
    {!! Form::label('father_name', 'Father\'s Name *', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12', 'required' => 'required']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('father_name', null, ['class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Enter Father\'s Name']) !!}
        {!! $errors->first('father_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="item form-group {{ $errors->has('mother_name') ? 'has-error' : ''}}">
    {!! Form::label('mother_name', 'Mother\'s Name *', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('mother_name', null, ['class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Enter Mother\'s Name', 'required' => 'required']) !!}
        {!! $errors->first('mother_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="item form-group {{ $errors->has('dob') ? 'has-error' : ''}}">
    {!! Form::label('dob', 'Date of Birth *', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::date('dob', null, ['class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Enter Date of Birth', 'required' => 'required']) !!}
        {!! $errors->first('dob', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="item form-group {{ $errors->has('gender') ? 'has-error' : ''}}">
    {!! Form::label('gender', 'Gender *', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::radio('gender', 'male', ['class' => 'form-control col-md-7 col-xs-12', 'required' => 'required']) !!} Male
        {!! Form::radio('gender', 'female', ['class' => 'form-control col-md-7 col-xs-12', 'required' => 'required']) !!} Female
        {!! Form::radio('gender', 'others', ['class' => 'form-control col-md-7 col-xs-12', 'required' => 'required']) !!} Others
        {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="item form-group {{ $errors->has('marital_status') ? 'has-error' : ''}}">
    {!! Form::label('marital_status', 'Marital Status *', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select('marital_status', ['married' => 'Married', 'single' => 'Single', 'divorcee' => 'Divorcee', 'others' => 'Others'] , null, ['class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Enter Marital Status', 'required' => 'required']) !!}
        {!! $errors->first('marital_status', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="item form-group {{ $errors->has('religion') ? 'has-error' : ''}}">
    {!! Form::label('religion', 'Marital Status *', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select('religion', ['islam' => 'Islam', 'hinduism' => 'Hinduism', 'christianity' => 'Christianity', 'buddhism' => 'Buddhism', 'others' => 'Others'] , null, ['class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Enter Religion', 'required' => 'required']) !!}
        {!! $errors->first('religion', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="item form-group {{ $errors->has('occupation') ? 'has-error' : ''}}">
    {!! Form::label('occupation', 'Occupation *', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('occupation', null, ['class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Occupation', 'required' => 'required']) !!}
        {!! $errors->first('occupation', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="item form-group {{ $errors->has('occupation_details') ? 'has-error' : ''}}">
    {!! Form::label('occupation_details', 'Occupation Details *', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::textarea('occupation_details',null,['class'=>'form-control col-md-7 col-xs-12', 'placeholder' => 'Enter Occupation Details', 'data-validate-length-range' => '20,500', 'rows' => 2, 'cols' => 40]) !!}
        {!! $errors->first('occupation_details', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="item form-group {{ $errors->has('phone_number') ? 'has-error' : ''}}">
    {!! Form::label('phone_number', 'Phone Number *', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('phone_number', null, ['class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Enter Phone Number', 'required' => 'required']) !!}
        {!! $errors->first('phone_number', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@if(!isset($user))
<div class="item form-group {{ $errors->has('nid') ? 'has-error' : ''}}">
    {!! Form::label('nid', 'NID *', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('nid', null, ['class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Enter NID', 'required' => 'required']) !!}
        {!! $errors->first('nid', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@endif
<div class="item form-group {{ $errors->has('permanent_address') ? 'has-error' : ''}}">
    {!! Form::label('permanent_address', 'Permanent Address *', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::textarea('permanent_address',null,['class'=>'form-control col-md-7 col-xs-12', 'placeholder' => 'Enter Permanent Address', 'data-validate-length-range' => '20,500', 'rows' => 2, 'cols' => 40]) !!}
        {!! $errors->first('permanent_address', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="ln_solid"></div>
<div class="form-group">
    <div class="col-md-6 col-md-offset-3">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-success']) !!}
    </div>
</div>

