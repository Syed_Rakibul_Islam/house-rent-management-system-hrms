@extends('layouts.app')

@section('style')
    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset('design/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div class="row top_tiles">
        @if(auth()->user()->type == 'admin')
            <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-users"></i></div>
                    <div class="count">{{ $users }}</div>
                    <h3>Total Users</h3>
                </div>
            </div>
            <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-home"></i></div>
                    <div class="count">{{ $houses }}</div>
                    <h3>Total Houses</h3>
                </div>
            </div>
            <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-windows"></i></div>
                    <div class="count">{{ $flats }}</div>
                    <h3>Total Flats</h3>
                </div>
            </div>
        @else
            <div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fa fa-home"></i></div>
                    <div class="count">{{ $houses }}</div>
                    <h3>Total House{{ $houses > 0 ? 's' : '' }}</h3>
                </div>
            </div>
        @endif
    </div>
@endsection


@section('script')
    <!-- Chart.js -->
    <script src="{{ asset('design/vendors/Chart.js/dist/Chart.min.js') }}"></script>
    <!-- jQuery Sparklines -->
    <script src="{{ asset('design/vendors/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
    <!-- Flot -->
    <script src="{{ asset('design/vendors/Flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('design/vendors/Flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('design/vendors/Flot/jquery.flot.time.js') }}"></script>
    <script src="{{ asset('design/vendors/Flot/jquery.flot.stack.js') }}"></script>
    <script src="{{ asset('design/vendors/Flot/jquery.flot.resize.js') }}"></script>
    <!-- Flot plugins -->
    <script src="{{ asset('design/vendors/flot.orderbars/js/jquery.flot.orderBars.js') }}"></script>
    <script src="{{ asset('design/vendors/flot-spline/js/jquery.flot.spline.min.js') }}"></script>
    <script src="{{ asset('design/vendors/flot.curvedlines/curvedLines.js') }}"></script>
    <!-- DateJS -->
    <script src="{{ asset('design/vendors/DateJS/build/date.js') }}"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="{{ asset('design/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('design/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
@endsection