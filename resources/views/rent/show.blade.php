@extends('layouts.app')
@section('content')
    <div class="clearfix"></div>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>View Rents - {{ $rent->id }}</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <a href="{{ url('/my_renting/') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i></button></a>
                @if($rent->renter_approve == null)
                    {!! Form::open([
                    'method'=>'PUT',
                    'url' => ['/my_renting', $rent->id],
                    'style' => 'display:inline'
                    ]) !!}
                    {!! Form::button('<i class="fa fa-check" aria-hidden="true"></i>', array(
                    'type' => 'submit',
                    'class' => 'btn btn-success btn-xs',
                    'title' => 'Approve Rent!',
                    'onclick'=>'return confirm("Confirm Approve?")'
                    )) !!}
                    {!! Form::close() !!}
                @endif
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <th>ID</th><td>{{ $rent->id }}</td>
                    </tr>
                    <tr>
                        <th>House Number</th><td>{{ $rent->flat->house->house_no }}</td>
                    </tr>
                    <tr>
                        <th>House Name</th><td>{{ $rent->flat->house->name }}</td>
                    </tr>
                    <tr>
                        <th>Flat Name</th><td>{{ $rent->flat->name }}</td>
                    </tr>
                    <tr>
                        <th>Renter Name</th><td>{{ $rent->renter->name }}</td>
                    </tr>
                    <tr>
                        <th>Started Date</th><td>{{ $rent->start_date }}</td>
                    </tr>
                    <tr>
                        <th>End Date</th><td>{{ $rent->end_date }}</td>
                    </tr>
                    <tr>
                        <th>Created Date</th><td>{{ Carbon\Carbon::parse($rent->created_at)->format('l jS F y') }}</td>
                    </tr>
                    <tr>
                        <th>Updated Date</th><td>{{ Carbon\Carbon::parse($rent->updated_at)->format('l jS F y') }}</td>
                    </tr>
                    <tr>
                        <th>Status</th>
                        @if($rent->renter_approve == null)
                            <td class="bg-danger">Pending</td>
                        @else
                            <td class="bg-success">Approved</td>
                        @endif
                    </tr>

                    </tbody>
                </table>

            </div>
        </div>
    </div>
@endsection
