<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = ['name', 'title', 'url', 'status', 'type', 'user_id'];
}
