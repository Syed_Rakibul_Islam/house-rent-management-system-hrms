<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function index()
    {
        return redirect('/');
    }

    public function show($id){
        $notification = Notification::where('user_id', auth()->user()->id)->findOrFail($id);
        $notification->update(['status' => 1]);
        return redirect($notification->url);
    }
}
