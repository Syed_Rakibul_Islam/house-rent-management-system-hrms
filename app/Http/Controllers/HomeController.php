<?php

namespace App\Http\Controllers;

use App\Flat;
use App\House;
use App\User;
use Illuminate\Http\Request;
use Session;
use App\Http\Controllers\Controller;
use App\Role;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->type == 'admin'){
            $users = User::whereNull('type')->count();
            $houses = House::count();
            $flats = Flat::count();
            return view('home', compact('users','houses', 'flats'));
        }
        $houses = House::where('user_id', auth()->user()->id)->count();
        return view('home', compact('houses'));
    }
    public function test()
    {
        return view('test');
    }
}
