<?php

namespace App\Http\Controllers\Rent;

use App\Notification;
use App\Rent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Session;

class RentController extends Controller
{
    public function index(){
        $rents = Rent::where('renter_id', auth()->user()->id)->whereNull('end_date')->with('flat.house')->get();
        return view('rent.index', compact('rents'));
    }

    public function show($id){
        $rent = Rent::where('renter_id', auth()->user()->id)->findOrFail($id);
        return view('rent.show', compact('rent'));
    }

    public function approve($id){
        $rent = Rent::where('renter_id', auth()->user()->id)->findOrFail($id);
        $rent->update(['renter_approve' => 1]);
        Notification::create([
            'name' => Auth::user()->name . ' has been approved to take your flat for renting.',
            'type' => 'rent_approve',
            'url' => 'house/renter/' . $rent->id,
            'user_id' => $rent->user_id
        ]);
        Session::flash('flash_success_msg', 'Rent approved!');
        return redirect('my_renting');
    }

    public function previousRenting(){
        $rents = Rent::where('renter_id', auth()->user()->id)->whereNotNull('end_date')->with('flat.house')->get();
        return view('rent.previous-rent', compact('rents'));
    }
}
