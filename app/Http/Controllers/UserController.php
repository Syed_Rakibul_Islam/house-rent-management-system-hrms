<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Session;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::whereNull('type')->get();
        return view('user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'username' => 'required|max:100|unique:users',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|min:6|max:255',
            'father_name' => 'required|string|max:100',
            'mother_name' => 'required|string|max:100',
            'dob' => 'required|date',
            'gender' => 'required',
            'marital_status' => 'required',
            'religion' => 'required',
            'occupation_details' => 'required|string|max:500',
            'phone_number' => 'required|max:100',
            'nid' => 'string|required|max:50|unique:users',
            'permanent_address' => 'required|string|max:500',
        ]);
        $request->request->add(['active' => 1]);
        $request['password'] = bcrypt($request->password);
        User::create($request->all());
        Session::flash('flash_success_msg', 'User Created!');
        return redirect('user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('user.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $this->validate($request, [
            'name' => 'required|string|max:100',
            'father_name' => 'required|string|max:100',
            'mother_name' => 'required|string|max:100',
            'dob' => 'required|date',
            'gender' => 'required',
            'marital_status' => 'required',
            'religion' => 'required',
            'occupation_details' => 'required|string|max:500',
            'phone_number' => 'required|max:100',
            'permanent_address' => 'required|string|max:500',
        ]);
        $user->update($request->all());
        Session::flash('flash_success_msg', 'User Updated!');
        return redirect('user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        Session::flash('flash_success_msg', 'User Deleted!');
        return redirect('user');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function active($id)
    {
        $user = User::findOrFail($id);
        $user->update(['active' => 1]);

        Session::flash('flash_success_msg', 'User Activated!');
        return redirect('user');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function suspend($id)
    {
        $user = User::findOrFail($id);
        $user->update(['active' => 0]);

        Session::flash('flash_success_msg', 'User Suspended!');
        return redirect('user');
    }
}
